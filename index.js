var path = require('path')
var express = require('express')
var mysql   = require('mysql');
var app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.set('port', (process.env.PORT || 3000))
app.use(express.static(__dirname + '/public'))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
  res.send('Hello NodeJS!')
})

var login = require('./src/login');
app.use('/login', function(req, res, next) {
  if (req.method == 'GET') {
    res.render('login');
  } else {
    if (login.checkPsw(req.body.password)) {
      var helloMsg = login.getHelloMsg(req.body.username);
      return res.status(200).send({
        msg: helloMsg
      });
    } else {
      return res.status(401).send({
        msg: "Please try it again."
      });
    }
  }
})

app.post('/profile', function(req, res, next) {
  var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'me',
    password : 'secret',
    database : 'my_db'
  });

  connection.connect();
  const usrId = req.body.usrId;
  // SEC ISSUE: SQL Injection
  const sql = 'SELECT * FROM users WHERE usrId =' + usrId;
  connection.query(sql, (err, rows) => {
    if (err) {
      connection.end();
      throw err;
    }
    connection.end();
    return res.json({ data: rows });
  });
})

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'))
})

module.exports = app